import "./navBar.scss";
import { Link } from "react-router-dom";

const NavBar = ({ menuOpen, setMenuOpen }) => {
  return (
    <div className={"navbar " + (menuOpen && "active")}>
      <div className="wrapper">
        <div className="left">
          <Link href="#intro" className="logo">
            <img src="images/logo.svg" alt="logo" />
          </Link>
        </div>
        <div className="right">
          <div className="hamburgerbar" onClick={() => setMenuOpen(!menuOpen)}>
            <span className="line1"></span>
            <span className="line2"></span>
            <span className="line3"></span>
          </div>
          <ul className="links">
            <li>
              <Link exact to="/">
                Home
              </Link>
            </li>
            <li>
              <Link exact to="/work">
                Works
              </Link>
            </li>
            <li>
              <Link exact to="/portfolio">
                Portfolio
              </Link>
            </li>
            <li>
              <Link exact to="/contact">
                Contact
              </Link>
            </li>
          </ul>
          <Link exact to="/login" className="login">
            <i class="fas fa-user-circle"></i>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
