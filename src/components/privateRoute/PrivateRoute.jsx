import React from "react";
import { Route, Redirect } from "react-router-dom";
import isAuthenticated from "../../helper/authHelper";
import "./userEntry.scss";
const PrivateRoute = ({ children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={() => (isAuthenticated() ? children : <Redirect to="/login" />)}
    />
  );
};

export default PrivateRoute;
