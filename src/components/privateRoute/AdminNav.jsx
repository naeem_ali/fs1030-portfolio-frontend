import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./admin.scss";
import { useHistory } from "react-router-dom";
const AdminNav = () => {
  let history = useHistory();
  const [userLogin, setUserLogin] = useState("");

  useEffect(() => {
    setUserLogin(
      JSON.parse(atob(sessionStorage.getItem("token").split(".")[1]))
    );
  }, []);

  const logout = (e) => {
    e.preventDefault();
    sessionStorage.removeItem("token");
    history.push("/login");
  };

  return (
    <>
      <div className="nav-con">
        <div className="userName">
          <i class="fas fa-user-circle"></i>
          <h4>{userLogin.username}</h4>
        </div>
        <ul className="nav">
          <li className="nav-item">
            <Link
              className="nav-link"
              id="nav-link"
              exact
              to="/admin/userEntry"
            >
              User Entries
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" exact to="/admin/admin-portfolio">
              Portfolio
            </Link>
          </li>
          <li className="nav-item">
            <Link exact className="nav-link" to="/admin/resume">
              Resume
            </Link>
          </li>

          <li className="nav-item">
            <Link
              exact
              className="nav-link logout-btn"
              to="/login"
              onClick={logout}
            >
              Logout
            </Link>
          </li>
        </ul>
      </div>
    </>
  );
};

export default AdminNav;
