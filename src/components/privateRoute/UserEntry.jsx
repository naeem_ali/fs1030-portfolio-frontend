import React, { useEffect, useState } from "react";
import "./userEntry.scss";
import AdminNav from "./AdminNav";
import axios from "axios";
import Session from "./Session";

const UserEntry = () => {
  const [userListing, setUserListing] = useState([]);

  const token = sessionStorage.getItem("token");

  useEffect(() => {
    axios({
      method: "get",
      url: "https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/userEntries",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(function (response) {
        setUserListing(response.data);
      })
      .catch((err) => {
        console.log({ err });
      });
  }, [token]);

  return (
    <div className=" usersData">
      <Session />
      <AdminNav />
      <table class="table">
        <thead class="table-striped table-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Message</th>
            <th scope="col">Date</th>
          </tr>
        </thead>
        <tbody>
          {userListing.map((user) => (
            <tr className="tab_hover" key={user.U_id}>
              <th scope="row">{user.U_id}</th>
              <td>{user.name}</td>
              <td>{user.email}</td>
              <td>{user.message}</td>
              <td>{new Date(user.entry_date).toDateString()}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default UserEntry;
