import AdminNav from "../AdminNav";
import "./adminPortfolio.scss";
import { Link } from "react-router-dom";
import axios from "axios";
import React, { useState, useEffect } from "react";
import Session from "../Session";
import "../userEntry.scss";

const AdminPortfolio = () => {
  const [portfolio, setPortfolio] = useState([]);

  const token = sessionStorage.getItem("token");

  useEffect(() => {
    loadPortfolio();
    /* eslint-disable-next-line */
  }, []);

  const loadPortfolio = async () => {
    const result = await axios({
      method: "get",
      url: "https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/portfolio",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    setPortfolio(result.data);
  };

  const onDelPortfolio = async (id) => {
    await axios.delete(
      `https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/portfolio/${id}`
    );
    loadPortfolio();
  };

  return (
    <div className="ad_portfolio">
      <Session />
      <AdminNav />
      <div className="ad_p_heading">
        <h2>Portfolio List </h2>
        <Link to="/admin/add-portfolio" class="btn btn-outline-success add_btn">
          + Add
        </Link>
      </div>

      <div>
        {portfolio.map((item) => (
          <Link
            to={`/admin/admin-portfolio/${item.P_id}`}
            className="veiw-portfolio"
          >
            <div className="portfolio_con">
              <div className="img_con">
                <img src={item.image} alt="" width="150px" height="150px" />
              </div>
              <div className="portfolio_content">
                <h4>{item.title}</h4>
                <h6>{item.category}</h6>
                <p>{item.description}</p>
              </div>
              <div className="btn-group-vertical curd_btn">
                <Link
                  to={`/admin/edit-portfolio/${item.P_id}`}
                  className="btn edit_btn"
                >
                  <i class="far fa-edit"></i>
                </Link>
                <Link
                  className="btn del_btn"
                  onClick={() => onDelPortfolio(item.P_id)}
                >
                  <i class="far fa-trash-alt"></i>
                </Link>
              </div>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default AdminPortfolio;
