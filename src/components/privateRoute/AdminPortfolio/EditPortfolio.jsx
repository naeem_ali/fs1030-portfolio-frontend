import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import axios from "axios";
import "./adminPortfolio.scss";
import "../userEntry.scss";

const EditPortfolio = () => {
  let history = useHistory();
  const { id } = useParams();
  const token = sessionStorage.getItem("token");

  const [edPortfolio, setEdPortfolio] = useState({
    P_id: "",
    title: "",
    category: "",
    image: "",
    description: "",
  });

  useEffect(() => {
    loadPortfolio();
    /* eslint-disable-next-line */
  }, []);

  const inputChange = (event) => {
    setEdPortfolio((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));

    //setEdPortfolio({ ...edPortfolio, [event.target.name]: event.target.value });
  };

  const { P_id, title, category, image, description } = edPortfolio;

  const onSubmitform = async (e) => {
    e.preventDefault();
    await axios.put(
      `https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/portfolio/${id}`,
      edPortfolio
    );

    history.push("/admin/admin-portfolio");
  };

  const loadPortfolio = async () => {
    const result = await axios({
      method: "get",
      url: `https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/portfolio/${id}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const payload = result.data;
    setEdPortfolio(...payload);
    console.log(...payload);
    console.log(P_id);
  };

  return (
    <div>
      <div>
        <button
          className="btn btn-outline-primary back_btn"
          onClick={() => history.push("/admin/admin-portfolio")}
        >
          <i class="fas fa-chevron-left"></i> <span>Back</span>
        </button>
      </div>

      <div className="add_portfolio_con">
        <div className="pt-heading">
          <h2> Add Portfolio </h2>
        </div>

        <hr />

        <div className="form_con">
          <form onSubmit={(e) => onSubmitform(e)}>
            <div className="form-group">
              <label htmlFor="ID">ID</label>
              <input
                type="text"
                className="form-control P_id"
                id="P_id"
                name="P_id"
                value={P_id}
                onChange={(e) => inputChange(e)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="title">Title</label>
              <input
                type="text"
                className="form-control title"
                id="title"
                name="title"
                value={title}
                onChange={(e) => inputChange(e)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="category">Category</label>
              <select
                className="form-control category "
                name="category"
                value={category}
                id="category"
                onChange={(e) => inputChange(e)}
              >
                <option disabled defaultValue selected>
                  -- Select a Category --
                </option>
                <option>Web</option>
                <option>Mobile</option>
                <option>Design</option>
                <option>Branding</option>
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="image">Image URL</label>
              <input
                type="text"
                className="form-control title"
                id="image"
                name="image"
                value={image}
                onChange={(e) => inputChange(e)}
              />
            </div>

            <div className="form-group">
              <label htmlFor="description">Description</label>
              <textarea
                className="form-control description"
                id="description"
                name="description"
                value={description}
                onChange={(e) => inputChange(e)}
                rows="3"
              ></textarea>
            </div>

            <div className="d-grid gap-2 col-12 mx-auto form-group">
              <button className="btn btn-success btn-lg" type="submit">
                Edit Portfolio
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default EditPortfolio;
