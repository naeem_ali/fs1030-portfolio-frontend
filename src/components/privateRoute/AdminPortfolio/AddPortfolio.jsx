import React, { useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import "./adminPortfolio.scss";

const AddPortfolio = () => {
  const history = useHistory();
  const [addPortfolio, setAddPortfolio] = useState({
    P_id: "",
    title: "",
    category: "",
    image: "",
    description: "",
  });

  const inputChange = (event) => {
    setAddPortfolio({
      ...addPortfolio,
      [event.target.name]: event.target.value,
    });
  };

  const { title, category, image, description } = addPortfolio;

  const submitInput = async (e) => {
    e.preventDefault();
    await axios.post(
      "https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/portfolio",
      addPortfolio
    );
    history.push("/admin/admin-portfolio");
  };

  return (
    <div>
      <div>
        <button
          className="btn btn-outline-primary back_btn"
          onClick={() => history.push("/admin/admin-portfolio")}
        >
          <i class="fas fa-chevron-left"></i> <span>Back</span>
        </button>
      </div>
      <div className="add_portfolio_con">
        <div className="pt-heading">
          <h2> Add Portfolio </h2>
        </div>
        <hr />
        <div className="form_con">
          <form onSubmit={(e) => submitInput(e)}>
            <div className="form-group">
              <label htmlFor="title">Title</label>
              <input
                type="text"
                className="form-control title"
                id="title"
                name="title"
                value={title}
                placeholder="Portfolio Title"
                onChange={(e) => inputChange(e)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="category">Category</label>
              <select
                className="form-control category "
                name="category"
                value={category}
                id="category"
                onChange={(e) => inputChange(e)}
              >
                <option disabled selected value>
                  {" "}
                  -- Select a Category --{" "}
                </option>
                <option>Web</option>
                <option>Mobile</option>
                <option>Design</option>
                <option>Branding</option>
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="image">Image URL</label>
              <input
                type="text"
                className="form-control title"
                id="image"
                name="image"
                value={image}
                placeholder="Portfolio Title"
                onChange={(e) => inputChange(e)}
              />
            </div>

            <div className="form-group">
              <label htmlFor="description">Description</label>
              <textarea
                className="form-control description"
                id="description"
                name="description"
                value={description}
                onChange={(e) => inputChange(e)}
                rows="3"
              ></textarea>
            </div>

            <div className="d-grid gap-2 col-12 mx-auto form-group">
              <button className="btn btn-success btn-lg" type="submit">
                Add Portfolio
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AddPortfolio;
