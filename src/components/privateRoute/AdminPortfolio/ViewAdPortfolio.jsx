import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import axios from "axios";
import "./adminPortfolio.scss";
import Session from "../Session";
import "../userEntry.scss";

const ViewAdPortfolio = () => {
  const { id } = useParams();
  const history = useHistory();
  const token = sessionStorage.getItem("token");

  const [pdata, setPdata] = useState([]);

  useEffect(() => {
    loadPortfolio();
    /* eslint-disable-next-line */
  }, []);

  const loadPortfolio = async () => {
    const result = await axios({
      method: "get",
      url: `https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/portfolio/${id}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    setPdata(result.data);
    console.log(result.data);
  };

  return (
    <div>
      <Session />
      <div>
        <button
          className="btn btn-outline-primary back_btn"
          onClick={() => history.push("/admin/admin-portfolio")}
        >
          <i class="fas fa-chevron-left"></i> <span>Back</span>
        </button>
      </div>
      {pdata.map((p) => (
        <div className="portfolioView" key={p.P_id}>
          <div className="img-con">
            <img src={p.image} alt="" width="800px" />
          </div>
          <div className="portfolio-detail">
            <h4>{p.title}</h4>
            <h6>{p.category}</h6>
            <p>{p.description}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default ViewAdPortfolio;
