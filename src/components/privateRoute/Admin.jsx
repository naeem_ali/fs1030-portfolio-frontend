import React from "react";
import "./admin.scss";
import AdminNav from "./AdminNav";

const Admin = () => {
  return (
    <div className="admin">
      <AdminNav />
    </div>
  );
};

export default Admin;
