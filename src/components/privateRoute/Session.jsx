import React, { useEffect, useState } from "react";
import "./userEntry.scss";
import axios from "axios";
import { useHistory } from "react-router-dom";

const Session = () => {
  const [sessionExp, setSessionExp] = useState(false);
  const token = sessionStorage.getItem("token");
  let history = useHistory();

  const onSessionExp = (e) => {
    e.preventDefault();
    sessionStorage.removeItem("token");
    history.push("/login");
  };

  useEffect(() => {
    axios({
      method: "get",
      url: "https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/user",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(function (res) {
        return res;
      })
      .catch((err) => {
        console.log({ err });
        if (err.response.status >= 400) {
          setSessionExp(true);
        }
      });
  }, [token]);

  return (
    <div>
      {sessionExp && (
        <div className="alert_con">
          <div className="alert alert-danger" role="alert">
            <h5 className="alert-heading">Session Expire!</h5>
            <hr />
            <p>Sorry, your session has expired, please login again.</p>
            <button className="btn btn-danger" onClick={onSessionExp}>
              OK, Got it!
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Session;
