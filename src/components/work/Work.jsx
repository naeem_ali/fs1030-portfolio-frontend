import React, { useState } from "react";
import { work, education } from "../resumeData";
import { workLeftVariant, workRightVariant } from "./../anim";

import "./work.scss";
import { motion } from "framer-motion";

const Work = () => {
  const [tab, setTab] = useState(1);

  const handelClick = (index) => {
    setTab(index);
  };

  return (
    <div className="work" id="work">
      <div className="work-container" id="resume">
        <motion.div
          className="anim-div"
          variants={workLeftVariant}
          initial="hidden"
          animate="visible"
        >
          <div className="left">
            <img src="images/illus-ex.png" alt="" />
          </div>
        </motion.div>

        <motion.div
          className="anim-div"
          variants={workRightVariant}
          initial="hidden"
          animate="visible"
        >
          <div className="right">
            {/* <h2>
            Work <span>Experience</span>
          </h2> */}

            <div className="resume-con">
              <ul className="resumeNav">
                <li
                  className={"resumeLink " + (tab === 1 && "rLink-1-active")}
                  onClick={() => handelClick(1)}
                >
                  Work Experience
                </li>
                <li
                  className={"resumeLink " + (tab === 2 && "rLink-1-active")}
                  onClick={() => handelClick(2)}
                >
                  Education
                </li>
                <li
                  className={"resumeLink " + (tab === 3 && "rLink-1-active")}
                  onClick={() => handelClick(3)}
                >
                  Skills
                </li>
              </ul>
            </div>

            <div className={"content-work" + (tab === 1 && "active")}>
              {tab === 1 && (
                <div className="work-con">
                  {work.map((exp) => (
                    <div className="exp-box">
                      <div className="exphead">
                        <h4>{exp.company}</h4>
                        <p>{exp.date}</p>
                      </div>
                      <h6>{exp.position}</h6>
                      <p>{exp.description}</p>
                    </div>
                  ))}
                </div>
              )}
            </div>
            <div className={"content-edu" + (tab === 2 && "edu-active")}>
              {tab === 2 && (
                <div className="edu-con">
                  {education.map((edu) => (
                    <div className="edu-box">
                      <div className="eduhead">
                        <h4>{edu.Degree}</h4>
                        <p>{edu.date}</p>
                      </div>
                      <h6>{edu.Institution}</h6>
                    </div>
                  ))}
                </div>
              )}
            </div>
            <div className={"content-skills" + (tab === 3 && "skill-active")}>
              {tab === 3 && (
                <div>
                  <motion.div
                    className="skill-con"
                    whileHover={{
                      height: "200px",
                    }}
                  >
                    <div className="skill">
                      <h3>Front-End</h3>
                      <i class="fas fa-chevron-down"></i>
                    </div>
                    <div className="items-con">
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        transition={{
                          type: "spring",
                          stiffness: 500,
                          damping: 30,
                        }}
                        className="skillBox"
                      >
                        <i class="fab fa-html5 icon-html"></i>
                      </motion.div>
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        className="skillBox"
                      >
                        <i class="fab fa-css3-alt icon-css"></i>
                      </motion.div>
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        className="skillBox"
                      >
                        <i class="fab fa-js icon-js"></i>
                      </motion.div>
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        className="skillBox"
                      >
                        <i class="fab fa-react icon-react"></i>
                      </motion.div>

                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        className="skillBox"
                      >
                        <i class="fab fa-sass icon-sass"></i>
                      </motion.div>
                    </div>
                  </motion.div>

                  <motion.div
                    className="skill-con"
                    whileHover={{
                      height: "200px",
                    }}
                  >
                    <div className="skill">
                      <h3>Back-End / Database</h3>
                      <i class="fas fa-chevron-down"></i>
                    </div>
                    <div className="items-con">
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        transition={{
                          type: "spring",
                          stiffness: 500,
                          damping: 30,
                        }}
                        className="skillBox"
                      >
                        <i class="fab fa-node icon-node"></i>
                      </motion.div>
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        className="skillBox"
                      >
                        <p className="icon-ex">ex.js</p>
                      </motion.div>
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        className="skillBox"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="800"
                          height="40"
                          viewBox="0 0 9.252 4.626"
                        >
                          <g
                            transform="matrix(.037376 0 0 .037376 1.069994 -1.319339)"
                            fill-rule="evenodd"
                          >
                            <path
                              d="M8.504 128.215h5.8v-22.977l9.058 20.033c1.026 2.408 2.5 3.3 5.354 3.3s4.24-.893 5.3-3.3l9.013-20.033v22.977h5.845v-22.977c0-2.23-.893-3.303-2.767-3.883-4.417-1.338-7.362-.178-8.7 2.81l-8.878 19.81-8.567-19.81c-1.294-2.988-4.284-4.148-8.745-2.81-1.83.58-2.722 1.652-2.722 3.883l-.001 22.977zm45.198-18.694h5.845v12.627c-.044.713.223 2.32 3.4 2.363 1.65.045 12.582 0 12.67 0v-15.08h5.845v20.658c0 5.086-6.3 6.2-9.236 6.246h-18.38v-3.88h18.427c3.748-.402 3.302-2.275 3.302-2.9v-1.518h-12.36c-5.756-.045-9.46-2.588-9.503-5.488v-13.03zm125.374-14.635c-3.568-.09-6.336.268-8.656 1.25-.668.27-1.74.27-1.828 1.116.357.355.4.936.713 1.428.535.893 1.473 2.096 2.32 2.72.938.715 1.875 1.428 2.855 2.053 1.74 1.07 3.703 1.695 5.398 2.766.982.625 1.963 1.428 2.945 2.098.5.357.803.938 1.428 1.16v-.135c-.312-.4-.402-.98-.713-1.428-.447-.445-.893-.848-1.34-1.293-1.293-1.74-2.9-3.258-4.64-4.506-1.428-.982-4.55-2.32-5.13-3.97l-.088-.09c.98-.09 2.14-.447 3.078-.715 1.518-.4 2.9-.312 4.46-.713.715-.18 1.428-.402 2.143-.625v-.4c-.803-.803-1.383-1.874-2.23-2.632-2.275-1.963-4.775-3.882-7.363-5.488-1.383-.892-3.168-1.473-4.64-2.23-.537-.268-1.428-.402-1.74-.848-.805-.98-1.25-2.275-1.83-3.436-1.293-2.454-2.543-5.175-3.658-7.763-.803-1.74-1.295-3.48-2.275-5.086-4.596-7.585-9.594-12.18-17.268-16.687-1.65-.937-3.613-1.34-5.7-1.83l-3.346-.18c-.715-.312-1.428-1.16-2.053-1.562-2.543-1.606-9.102-5.086-10.977-.49-1.205 2.9 1.785 5.755 2.8 7.228.76 1.026 1.74 2.186 2.277 3.346.3.758.4 1.562.713 2.365.713 1.963 1.383 4.15 2.32 5.98.5.937 1.025 1.92 1.65 2.767.357.49.982.714 1.115 1.517-.625.893-.668 2.23-1.025 3.347-1.607 5.042-.982 11.288 1.293 14.99.715 1.115 2.4 3.57 4.686 2.632 2.008-.803 1.56-3.346 2.14-5.577.135-.535.045-.892.312-1.25v.09l1.83 3.703c1.383 2.186 3.793 4.462 5.8 5.98 1.07.803 1.918 2.187 3.256 2.677v-.135h-.088c-.268-.4-.67-.58-1.027-.892-.803-.803-1.695-1.785-2.32-2.677-1.873-2.498-3.523-5.265-4.996-8.12-.715-1.383-1.34-2.9-1.918-4.283-.27-.536-.27-1.34-.715-1.606-.67.98-1.65 1.83-2.143 3.034-.848 1.918-.936 4.283-1.248 6.737-.18.045-.1 0-.18.09-1.426-.356-1.918-1.83-2.453-3.078-1.338-3.168-1.562-8.254-.402-11.913.312-.937 1.652-3.882 1.117-4.774-.27-.848-1.16-1.338-1.652-2.008-.58-.848-1.203-1.918-1.605-2.855-1.07-2.5-1.605-5.265-2.766-7.764-.537-1.16-1.473-2.365-2.232-3.435-.848-1.205-1.783-2.053-2.453-3.48-.223-.49-.535-1.294-.178-1.83.088-.357.268-.49.623-.58.58-.49 2.232.134 2.812.4 1.65.67 3.033 1.294 4.416 2.23.625.446 1.295 1.294 2.098 1.518h.938c1.428.312 3.033.09 4.37.49 2.365.76 4.506 1.874 6.426 3.08 5.844 3.703 10.664 8.968 13.92 15.26.535 1.026.758 1.963 1.25 3.034.938 2.187 2.098 4.417 3.033 6.56.938 2.097 1.83 4.24 3.168 5.98.67.937 3.346 1.427 4.55 1.918.893.4 2.275.76 3.08 1.25 1.516.937 3.033 2.008 4.46 3.034.713.534 2.945 1.65 3.078 2.54zm-45.5-38.772a7.09 7.09 0 0 0-1.828.223v.09h.088c.357.714.982 1.205 1.428 1.83l1.027 2.142.088-.09c.625-.446.938-1.16.938-2.23-.268-.312-.312-.625-.535-.937-.268-.446-.848-.67-1.206-1.026z"
                              fill="#00678c"
                            />
                            <path
                              d="M85.916 128.215h16.776c1.963 0 3.838-.4 5.354-1.115 2.543-1.16 3.748-2.72 3.748-4.773v-4.283c0-1.65-1.383-3.213-4.148-4.283-1.428-.535-3.213-.848-4.953-.848h-7.05c-2.365 0-3.48-.715-3.793-2.275-.044-.178-.044-.357-.044-.535v-2.633c0-.135 0-.312.044-.49.312-1.205.937-1.518 3-1.74h17.177v-3.883h-16.33c-2.365 0-3.614.135-4.73.492-3.436 1.07-4.953 2.766-4.953 5.754v3.393c0 2.63 2.945 4.863 7.942 5.398.535.045 1.115.045 1.695.045h6.024c.223 0 .445 0 .623.045 1.83.178 2.633.49 3.168 1.158.357.357.447.67.447 1.072v3.39c0 .4-.268.938-.803 1.383s-1.385.758-2.5.803c-.223 0-.355.045-.58.045H85.916zm62.195-6.736c0 3.97 3 6.2 8.97 6.648.58.045 1.115.088 1.695.088h15.17v-3.88h-15.303c-3.393 0-4.686-.848-4.686-2.9v-20.078H148.1v20.123zm-32.615.177v-13.83c0-3.525 2.498-5.668 7.363-6.336.535-.045 1.07-.09 1.56-.09h11.064c.58 0 1.072.045 1.652.09 4.863.668 7.316 2.81 7.316 6.336v13.83c0 2.855-1.025 4.373-3.436 5.4l5.71 5.174h-6.736l-4.64-4.193-4.686.268h-6.246a13.66 13.66 0 0 1-3.391-.445c-3.7-1.028-5.53-2.99-5.53-6.204zm6.29-.31c0 .178.1.355.135.58.312 1.605 1.828 2.498 4.148 2.498h5.266l-4.818-4.373h6.736l4.238 3.838c.805-.447 1.295-1.072 1.473-1.875.045-.178.045-.4.045-.58v-13.252c0-.178 0-.355-.045-.535-.312-1.516-1.828-2.363-4.104-2.363h-8.79c-2.588 0-4.283 1.115-4.283 2.898z"
                              fill="#ce8b2c"
                            />
                          </g>
                        </svg>
                      </motion.div>
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        className="skillBox"
                      >
                        <div className="svg-mongo">
                          <svg
                            className="mongo"
                            xmlns="http://www.w3.org/2000/svg"
                            width="64"
                            height="64"
                            viewBox="0 0 32 32"
                          >
                            <path
                              d="M15.9.087l.854 1.604c.192.296.4.558.645.802.715.715 1.394 1.464 2.004 2.266 1.447 1.9 2.423 4.01 3.12 6.292.418 1.394.645 2.824.662 4.27.07 4.323-1.412 8.035-4.4 11.12-.488.488-1.01.94-1.57 1.342-.296 0-.436-.227-.558-.436-.227-.383-.366-.82-.436-1.255-.105-.523-.174-1.046-.14-1.586v-.244C16.057 24.21 15.796.21 15.9.087z"
                              fill="#599636"
                            />
                            <path
                              d="M15.9.034c-.035-.07-.07-.017-.105.017.017.35-.105.662-.296.96-.21.296-.488.523-.767.767-1.55 1.342-2.77 2.963-3.747 4.776-1.3 2.44-1.97 5.055-2.16 7.808-.087.993.314 4.497.627 5.508.854 2.684 2.388 4.933 4.375 6.885.488.47 1.01.906 1.55 1.325.157 0 .174-.14.21-.244a4.78 4.78 0 0 0 .157-.68l.35-2.614L15.9.034z"
                              fill="#6cac48"
                            />
                            <path
                              d="M16.754 28.845c.035-.4.227-.732.436-1.063-.21-.087-.366-.26-.488-.453-.105-.174-.192-.383-.26-.575-.244-.732-.296-1.5-.366-2.248v-.453c-.087.07-.105.662-.105.75a17.37 17.37 0 0 1-.314 2.353c-.052.314-.087.627-.28.906 0 .035 0 .07.017.122.314.924.4 1.865.453 2.824v.35c0 .418-.017.33.33.47.14.052.296.07.436.174.105 0 .122-.087.122-.157l-.052-.575v-1.604c-.017-.28.035-.558.07-.82z"
                              fill="#c2bfbf"
                            />
                          </svg>
                        </div>
                      </motion.div>
                    </div>
                  </motion.div>

                  <motion.div
                    className="skill-con"
                    whileHover={{
                      height: "200px",
                    }}
                  >
                    <div className="skill">
                      <h3>Graphics</h3>
                      <i class="fas fa-chevron-down"></i>
                    </div>
                    <div className="items-con">
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        transition={{
                          type: "spring",
                          stiffness: 500,
                          damping: 30,
                        }}
                        className="skillBox"
                      >
                        <div className="svg-ps">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            enable-background="new 0 0 24 24"
                            viewBox="0 0 24 24"
                          >
                            <path
                              fill="#001D25"
                              d="M0,2v20c0,1.105,0.895,2,2,2h20c1.105,0,2-0.895,2-2V2c0-1.105-0.895-2-2-2H2C0.895,0,0,0.895,0,2z"
                            />
                            <path
                              fill="#00C8FE"
                              d="M22,1c0.551,0,1,0.449,1,1v20c0,0.551-0.449,1-1,1H2c-0.551,0-1-0.449-1-1V2c0-0.551,0.449-1,1-1H22L22,1z M22,0H2C0.896,0,0,0.895,0,2v20c0,1.105,0.896,2,2,2h20c1.104,0,2-0.895,2-2V2C24,0.895,23.104,0,22,0L22,0z"
                            />
                            <path
                              fill="#00C8FE"
                              d="M15.555 16.502c-.876 0-1.62-.183-2.05-.43-.065-.033-.08-.082-.08-.165V14.44c0-.1.048-.133.114-.084.624.413 1.352.594 2.012.594.792 0 1.122-.33 1.122-.776 0-.363-.23-.677-1.237-1.205-1.42-.68-2.014-1.37-2.014-2.527 0-1.287 1.006-2.36 2.755-2.36.86 0 1.464.132 1.794.28.082.05.1.132.1.198v1.37c0 .083-.05.133-.15.1-.444-.264-1.1-.43-1.743-.43l.004.004c-.792 0-1.056.396-1.056.726 0 .363.18.61 1.237 1.155 1.568.76 2.062 1.485 2.062 2.557C18.425 15.642 17.205 16.502 15.555 16.502zM8.602 5.314c-.99 0-1.934.017-2.578.05C5.94 5.364 5.8 5.413 5.8 5.48h.001v10.758c0 .083.032.113.115.113H7.46c.082 0 .115-.033.115-.116v-3.842c.265 0 .395.017.775.017 2.26 0 4.102-1.09 4.102-3.63C12.452 6.834 11.374 5.314 8.602 5.314zM8.385 10.808c-.397 0-.579 0-.809-.016V6.931c.182-.017.561-.033.957-.033 1.336 0 2.122.644 2.122 1.914C10.655 10.396 9.555 10.808 8.385 10.808z"
                            />
                          </svg>
                        </div>
                      </motion.div>
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        className="skillBox"
                      >
                        <svg
                          className="illus"
                          xmlns="http://www.w3.org/2000/svg"
                          enable-background="new 0 0 24 24"
                          viewBox="0 0 24 24"
                        >
                          <path
                            fill="#261200"
                            d="M22,0H2C0.896,0,0,0.895,0,2v20c0,1.105,0.896,2,2,2h20c1.104,0,2-0.895,2-2V2C24,0.895,23.104,0,22,0z"
                          />
                          <path
                            fill="#FF7C00"
                            d="M22,1c0.551,0,1,0.449,1,1v20c0,0.551-0.449,1-1,1H2c-0.551,0-1-0.449-1-1V2c0-0.551,0.449-1,1-1H22L22,1z M22,0H2C0.896,0,0,0.895,0,2v20c0,1.105,0.896,2,2,2h20c1.104,0,2-0.895,2-2V2C24,0.895,23.104,0,22,0L22,0z"
                          />
                          <path
                            fill="#FF7C00"
                            d="M17.531 16.242c0 .083-.016.116-.115.116h-1.55c-.1 0-.133-.05-.133-.13V8.34l-.008.001c0-.08.033-.11.116-.11h1.57c.088 0 .12.033.12.116V16.242zM16.619 7.09c-.6 0-1.01-.363-1.01-1.023h.004c0-.643.445-1.023 1.023-1.023.61 0 1.023.412 1.023 1.023C17.659 6.727 17.229 7.09 16.619 7.09zM11.05 5.424c-.016-.082-.05-.1-.116-.1H8.838c-.05 0-.083.034-.083.1C8.739 5.919 8.707 6.077 8.657 6.257l-2.84 9.945c-.015.115 0 .148.1.148h1.468c.098 0 .131-.019.148-.102l.792-2.998-.003-.001h3.207l.825 3.015c.016.066.065.086.148.086h1.65c.083 0 .1-.049.083-.132L11.05 5.424zM8.753 11.625c.413-1.55.941-3.53 1.155-4.7h.017c.248 1.172.908 3.594 1.188 4.7H8.753z"
                          />
                        </svg>
                      </motion.div>
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        className="skillBox"
                      >
                        <svg
                          className="xd"
                          xmlns="http://www.w3.org/2000/svg"
                          width="60"
                          height="60"
                          data-name="adobe xd"
                          viewBox="0 0 307.692 300"
                        >
                          <path
                            fill="#2e001e"
                            d="M10,10H292.051V284.359H10Z"
                            data-name="Path 12"
                            transform="translate(2.821 2.821)"
                          />
                          <path
                            fill="#ff2bc2"
                            d="M0,0V300H307.692V0ZM12.821,12.821H294.872V287.179H12.821Z"
                            data-name="Path 13"
                          />
                          <path
                            fill="#ff2bc2"
                            d="M210.711,110.372a17.6,17.6,0,0,0-7.436-1.282c-16.154,0-26.923,12.436-26.923,33.077,0,23.59,11.026,33.077,25.385,33.077a24.188,24.188,0,0,0,8.846-1.41V110.372Zm-57.436,32.821c0-29.872,19.231-53.205,50.769-53.205a56.853,56.853,0,0,1,6.667.256V56.91a1.285,1.285,0,0,1,1.282-1.282h20.641c1.026,0,1.282.385,1.282,1.026V173.833a91.315,91.315,0,0,0,.641,12.564c0,.9,0,1.026-.769,1.41a76.367,76.367,0,0,1-32.821,7.436C173.147,195.244,153.275,178.064,153.275,143.192Zm-43.718-20.769,35.9,69.615c.641,1.026.256,2.051-.769,2.051H122.378a2.516,2.516,0,0,1-2.692-1.667q-12.308-25.385-25.128-52.692H94.3c-7.692,17.18-16.154,35.9-24.359,52.821a2.462,2.462,0,0,1-2.308,1.41H46.352c-1.282,0-1.41-1.026-.769-1.795L80.711,124.6,46.737,57.423a1.135,1.135,0,0,1,.9-1.923H69.685c1.282,0,1.923.256,2.308,1.41,8.077,17.051,16.282,33.846,23.974,51.026h.256c7.436-16.923,15.641-33.974,23.462-50.769.641-1.026,1.026-1.667,2.308-1.667h20.641c1.026,0,1.41.769.769,1.923Z"
                            data-name="Path 14"
                            transform="translate(12.751 15.654)"
                          />
                        </svg>
                      </motion.div>
                      <motion.div
                        whileHover={{ scale: 1.2 }}
                        className="skillBox"
                      >
                        <svg
                          className="xd"
                          xmlns="http://www.w3.org/2000/svg"
                          enable-background="new 0 0 24 24"
                          viewBox="0 0 24 24"
                        >
                          <path
                            fill="#0ACF83"
                            d="M8,24c2.208,0,4-1.792,4-4v-4H8c-2.208,0-4,1.792-4,4S5.792,24,8,24z"
                          />
                          <path
                            fill="#A259FF"
                            d="M4,12c0-2.208,1.792-4,4-4h4v8H8C5.792,16,4,14.208,4,12z"
                          />
                          <path
                            fill="#F24E1E"
                            d="M4,4c0-2.208,1.792-4,4-4h4v8H8C5.792,8,4,6.208,4,4z"
                          />
                          <path
                            fill="#FF7262"
                            d="M12,0h4c2.208,0,4,1.792,4,4s-1.792,4-4,4h-4V0z"
                          />
                          <path
                            fill="#1ABCFE"
                            d="M20,12c0,2.208-1.792,4-4,4s-4-1.792-4-4s1.792-4,4-4S20,9.792,20,12z"
                          />
                        </svg>
                      </motion.div>
                    </div>
                  </motion.div>
                </div>
                // <div
                //   id="carouselExampleControls"
                //   className="carousel slide"
                //   data-ride="carousel"
                // >
                //   <div className="carousel-inner">
                //     <div className="carousel-item active">
                //       <h3>Front-End</h3>
                //       <div className="skillsName">
                //         <span className="skillBox">
                //           <i class="fab fa-html5 icon-html"></i>
                //         </span>
                //         <span className="skillBox">
                //           <i class="fab fa-css3-alt icon-css"></i>
                //         </span>
                //         <span className="skillBox">
                //           <i class="fab fa-js icon-js"></i>
                //         </span>
                //         <span className="skillBox">
                //           <i class="fab fa-react icon-react"></i>
                //         </span>
                //         <span className="skillBox">
                //           <i class="fab fa-sass icon-sass"></i>
                //         </span>
                //       </div>
                //     </div>
                //   </div>
                //   <a
                //     className="carousel-control-prev"
                //     href="#carouselExampleControls"
                //     role="button"
                //     data-slide="prev"
                //   >
                //     <span
                //       className="carousel-control-prev-icon"
                //       aria-hidden="true"
                //     ></span>
                //     <span className="sr-only">Previous</span>
                //   </a>
                //   <a
                //     className="carousel-control-next"
                //     href="#carouselExampleControls"
                //     role="button"
                //     data-slide="next"
                //   >
                //     <span
                //       className="carousel-control-next-icon"
                //       aria-hidden="true"
                //     ></span>
                //     <span className="sr-only">Next</span>
                //   </a>
                // </div>
              )}
            </div>
          </div>
        </motion.div>
      </div>
    </div>
  );
};

export default Work;
