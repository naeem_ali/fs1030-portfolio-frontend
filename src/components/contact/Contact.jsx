import "./contact.scss";
import { useFormik } from "formik";
import axios from "axios";
import { motion } from "framer-motion";
import { leftVariant, rightVariant } from "./../anim";

const initialValues = {
  name: "",
  email: "",
  message: "",
};

const onSubmit = (values) => {
  console.log("Form Data", values);
};

const validate = (values) => {
  let error = {};

  if (!values.name) {
    error.name = "Required";
  }
  if (!values.email) {
    error.email = "Required";
  } else if (
    /* eslint-disable-next-line */
    !/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
      values.email
    )
  ) {
    error.email = "Invalid email format";
  }
  if (!values.message) {
    error.message = "Required";
  }
  return error;
};

const Contact = () => {
  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });

  const submitInput = async () => {
    const name = document.getElementById("name").value;
    const email = document.getElementById("email").value;
    const message = document.getElementById("message").value;
    await axios.post("https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/userEntries", {
      name: name,
      email: email,
      message: message,
    });
  };

  return (
    <section className="contact" id="contact">
      <img src="images/ct.png" alt="" className="contact-bg" />
      <div className="contact-container">
        <motion.div
          className="anim-div"
          variants={leftVariant}
          initial="hidden"
          animate="visible"
        >
          <div className="contact-left">
            <div className="con-form">
              <h1>Contact</h1>
              <form action="">
                <input
                  type="text"
                  id="name"
                  placeholder="Name"
                  className="form-control inputName"
                  onChange={formik.handleChange}
                  value={formik.values.name}
                  onBlur={formik.handleBlur}
                />
                {formik.touched.name && formik.errors.name ? (
                  <div className="errorMsg-name">
                    <i class="far fa-exclamation-triangle"></i>
                    {formik.errors.name}
                  </div>
                ) : null}
                <input
                  type="email"
                  id="email"
                  placeholder="Email"
                  className="form-control inputEmail"
                  onChange={formik.handleChange}
                  value={formik.values.email}
                  onBlur={formik.handleBlur}
                />
                {formik.touched.email && formik.errors.email ? (
                  <div className="errorMsg-email">
                    <i class="far fa-exclamation-triangle"></i>
                    {formik.errors.email}
                  </div>
                ) : null}
                <textarea
                  name="message"
                  id="message"
                  cols="30"
                  rows="10"
                  placeholder="Please leave your message here"
                  className="form-control inputMessage"
                  onChange={formik.handleChange}
                  value={formik.values.message}
                  onBlur={formik.handleBlur}
                ></textarea>
                {formik.touched.message && formik.errors.message ? (
                  <div className="errorMsg-msg">
                    <i class="far fa-exclamation-triangle"></i>
                    {formik.errors.message}
                  </div>
                ) : null}
                <button
                  type="submit"
                  className="form-control inputSubmit"
                  onClick={(e) => submitInput(e)}
                >
                  SUBMIT
                </button>
              </form>
            </div>
          </div>
        </motion.div>

        <motion.div
          className="anim-div"
          variants={rightVariant}
          initial="hidden"
          animate="visible"
        >
          <div className="contact-right">
            <img src="images/illus-co.png" alt="" className="con-img" />
          </div>
        </motion.div>
      </div>
    </section>
  );
};

export default Contact;
