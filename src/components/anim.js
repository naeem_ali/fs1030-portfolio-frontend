export const leftVariant = {
  visible: {
    x: 0,
    opacity: 1,
    transition: { duration: 1, delay: 0.3, type: "spring", stiffness: 150 },
  },
  hidden: { opacity: 0, x: -100 },
};

export const rightVariant = {
  visible: {
    x: 0,
    opacity: 1,
    transition: { duration: 1, delay: 0.3, type: "spring", stiffness: 150 },
  },
  hidden: { opacity: 0, x: 100 },
};

export const workLeftVariant = {
  visible: {
    y: 0,
    opacity: 1,
    transition: { duration: 1, delay: 0.3, type: "spring", stiffness: 150 },
  },
  hidden: { opacity: 0, y: -50 },
};

export const workRightVariant = {
  visible: {
    y: 0,
    opacity: 1,
    transition: { duration: 1, delay: 0.3, type: "spring", stiffness: 150 },
  },
  hidden: { opacity: 0, y: 50 },
};

export const portfoliVariant1 = {
  visible: {
    y: 0,
    opacity: 1,
    transition: { duration: 1, delay: 0.3, type: "spring", stiffness: 150 },
  },
  hidden: { opacity: 0, y: -50 },
};

export const portfoliVariant2 = {
  visible: {
    y: 0,
    opacity: 1,
    transition: { duration: 1, delay: 0.3, type: "spring", stiffness: 150 },
  },
  hidden: { opacity: 0, y: 50 },
};
