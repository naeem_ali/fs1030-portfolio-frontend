export const work = [
  {
    id: 1,
    company: "Axact Ltd",
    position: "UI / UX Designer & Web Developer",
    description:
      "I have 5+ years of experience as a UX/UI designer specializing in visual design. During these years, I gained extensive experience in web design, layout development, mobile applications, and desktop (web) applications, and I've designed and built projects from different fields. Due to my extensive knowledge in HTML3, CSS, Javascript, and Javascript frameworks, I also develop some of my client projects. During these years I am committed to perfecting my craft by learning from more seasoned developers.",
    date: "May/2015",
  },

  {
    id: 2,
    company: "Otrack.io",
    position: "(Freelance Project)",
    description:
      "Design websites using WordPress and libraries like Jquery and create customized themes to meet clients’ requirements. Perform database and code optimization for WordPress using HTML5, CSS3, and JavaScript. Met coding standards and cross-browser compatibilities.",
    date: "August/2021",
  },
];

export const education = [
  {
    id: 1,
    Institution: "York University Continuing Studies",
    Degree: "Certificate in Full Stack Web Developer",
    date: "2021/01/01",
  },
  {
    id: 2,
    Institution: "University of Regina",
    Degree: "Diploma in Business Administration",
    date: "2015/09/01",
  },
  {
    id: 3,
    Institution: "Karachi School of Arts",
    Degree: "Diploma in Graphic Design",
    date: "2009/12/01",
  },
];

export const skill = [
  {
    id: 1,
    skill: "Adobe Photoshop",
    category: "Graphic",
    level: "intermediate",
    percent: "75",
    icon: "",
  },
  {
    id: 2,
    skill: "Adobe Illustrator",
    category: "Graphic",
    level: "intermediate",
    percent: "85",
    icon: "",
  },
  {
    id: 3,
    skill: "Adobe XD",
    category: "Graphic",
    level: "intermediate",
    percent: "70",
    icon: "",
  },
  {
    id: 4,
    skill: "HTML-5",
    category: "Wed Development",
    level: "intermediate",
    percent: "80",
    icon: "",
  },
  {
    id: 5,
    skill: "CSS-3",
    category: "Wed Development",
    level: "intermediate",
    percent: "80",
    icon: "",
  },
  {
    id: 6,
    skill: "Java Script",
    category: "Wed Development",
    level: "intermediate",
    percent: "70",
    icon: "",
  },
  {
    id: 7,
    skill: "Nodejs",
    category: "Wed Development",
    level: "Beginner",
    percent: "60",
    icon: "",
  },
  {
    id: 8,
    skill: "Expressjs",
    category: "Wed Development",
    level: "Beginner",
    percent: "65",
    icon: "",
  },
  {
    id: 9,
    skill: "MySql",
    category: "Database",
    level: "Beginner",
    percent: "65",
    icon: "",
  },
  {
    id: 10,
    skill: "Mongo DB",
    category: "Database",
    level: "Beginner",
    percent: "60",
    icon: "",
  },
  {
    id: 11,
    skill: "PHP",
    category: "Wed Development",
    level: "Beginner",
    percent: "40",
    icon: "",
  },
  {
    id: 12,
    skill: "Sass",
    category: "Wed Development",
    level: "Beginner",
    percent: "50",
    icon: "",
  },
  {
    id: 13,
    skill: "Reactjs",
    category: "Wed Development",
    level: "intermediate",
    percent: "80",
    icon: "",
  },
];
