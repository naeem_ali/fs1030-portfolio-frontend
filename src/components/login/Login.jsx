import React, { useState } from "react";
import "./login.scss";
import { useFormik } from "formik";
import axios from "axios";
import { useHistory } from "react-router-dom";

const initialValues = {
  email: "",
  password: "",
};

const onSubmit = (values) => {
  console.log("Form Data", values);
};

const validate = (values) => {
  let error = {};
// eslint-disable-next-line
  let regex = new RegExp("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");

  if (!values.email) {
    error.email = "Required";
  } else if (
    // eslint-disable-next-line
    !regex.test(values.email)

  ) {
    error.email = "Invalid email format";
  }
  if (!values.password) {
    error.password = "Required";
  }
  return error;
};

const Login = () => {
  const history = useHistory();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [auth, setauth] = useState(true);

  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });

  const userData = (e) => {
    e.preventDefault();

    const email = document.getElementById("exampleInputEmail1").value;
    const password = document.getElementById("exampleInputPassword1").value;

    axios
      .post("https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/user/login", {
        email: email,
        password: password,
      })
      .then((res) => {
        sessionStorage.setItem("token", res.data.token);

        history.push("/admin");
      })
      .catch((err) => {
        let statusCode = err.response.request.status;
        if (statusCode >= 400) {
          setauth(false);
        } else {
          setauth(true);
        }
      });
  };

  return (
    <div className="form-container">
      <form className="login-form" onSubmit={formik.handleSubmit}>
        <h3>Login</h3>
        <hr />
        <div className="form-group">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <input
            type="email"
            class="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            placeholder="Enter email"
            name="email"
            onChange={(formik.handleChange, (e) => setUsername(e.target.value))}
            value={(formik.values.email, username)}
            onBlur={formik.handleBlur}
          />
          {formik.touched.email && formik.errors.email ? (
            <div className="errorMsg-email">
              <i className="far fa-exclamation-triangle"></i>
              {formik.errors.email}
            </div>
          ) : null}
        </div>
        <div className="form-group">
          <label htmlFor="exampleInputPassword1">Password</label>
          <input
            type="password"
            class="form-control"
            id="exampleInputPassword1"
            placeholder="Password"
            name="password"
            onChange={(formik.handleChange, (e) => setPassword(e.target.value))}
            value={(formik.values.password, password)}
            onBlur={formik.handleBlur}
          />
          {formik.touched.password && formik.errors.password ? (
            <div className="errorMsg-pswd">
              <i className="far fa-exclamation-triangle"></i>
              {formik.errors.password}
            </div>
          ) : null}
        </div>

        <button type="submit" onClick={userData} class="btn btn-primary">
          Submit
        </button>
      </form>
      {!auth && (
        <h4 className="loginErr">Username or password is incorrect!</h4>
      )}
    </div>
  );
};

export default Login;
