import React from "react";
import "./registration.scss";
import { useFormik } from "formik";
import axios from "axios";
import { useHistory } from "react-router-dom";

const initialValues = {
  username: "",
  email: "",
  password: "",
  password2: "",
};

const onSubmit = (values) => {
  console.log("Form Data", values);
};

const validate = (values) => {
  let error = {};

  if (!values.username) {
    error.username = "Required";
  }
  if (!values.email) {
    error.email = "Required";
  } else if (
    /* eslint-disable-next-line */
    !/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
      values.email
    )
  ) {
    error.email = "Invalid email format";
  }
  if (!values.password) {
    error.password = "Required";
  } else if (values.password.length < 6) {
    error.password = "password must be at least 6 characters";
  }

  if (!values.password2) {
    error.password2 = "Required";
  }

  if (values.password !== values.password2) {
    error.password2 = "Both password must match";
  }

  return error;
};

const Registration = () => {
  const history = useHistory();

  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });

  const userData = (e) => {
    e.preventDefault();

    const username = document.getElementById("username").value;
    const email = document.getElementById("exampleInputEmail1").value;
    const password = document.getElementById("exampleInputPassword1").value;
    const password2 = document.getElementById("exampleInputPassword2").value;

    console.log(username + " " + email + " " + password + " " + password2);

    axios
      .post("https://portfoilo-back-end-ygpiv24hpa-nn.a.run.app/user/register", {
        username: username,
        email: email,
        password: password,
        password_confirm: password2,
      })
      .then((res) => {
        console.log(res);

        history.push("/login");
      })
      .catch((err) => {
        console.log(err.response.data.msg);
      });
  };

  return (
    <div className="form-container">
      <form className="login-form" onSubmit={formik.handleSubmit}>
        <h3>Registration</h3>
        <hr />
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <input
            type="username"
            className="form-control"
            id="username"
            aria-describedby="username"
            placeholder="Username"
            name="username"
            onChange={formik.handleChange}
            value={formik.values.username}
            onBlur={formik.handleBlur}
          />
          {formik.touched.username && formik.errors.username ? (
            <div className="errorMsg-username">
              <i className="far fa-exclamation-triangle"></i>
              {formik.errors.username}
            </div>
          ) : null}
        </div>
        <div className="form-group">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            placeholder="Enter email"
            name="email"
            onChange={formik.handleChange}
            value={formik.values.email}
            onBlur={formik.handleBlur}
          />
          {formik.touched.email && formik.errors.email ? (
            <div className="errorMsg-email-l">
              <i className="far fa-exclamation-triangle"></i>
              {formik.errors.email}
            </div>
          ) : null}
        </div>
        <div className="form-group">
          <label htmlFor="exampleInputPassword1">Password</label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword1"
            placeholder="Password"
            name="password"
            onChange={formik.handleChange}
            value={formik.values.password}
            onBlur={formik.handleBlur}
          />
          {formik.touched.password && formik.errors.password ? (
            <div className="errorMsg-password">
              <i className="far fa-exclamation-triangle"></i>
              {formik.errors.password}
            </div>
          ) : null}
        </div>

        <div className="form-group">
          <label htmlFor="exampleInputPassword2">Confirm Password</label>
          <input
            type="password"
            className="form-control"
            id="exampleInputPassword2"
            placeholder="Confirm Password"
            name="password2"
            onChange={formik.handleChange}
            value={formik.values.password2}
            onBlur={formik.handleBlur}
          />
          {formik.touched.password2 && formik.errors.password2 ? (
            <div className="errorMsg-password2">
              <i className="far fa-exclamation-triangle"></i>
              {formik.errors.password2}
            </div>
          ) : null}
        </div>

        <button type="submit" onClick={userData} class="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
};

export default Registration;
