import "./app.scss";
import { Switch, Route, useLocation } from "react-router-dom";
import NavBar from "./components/NavBar/NavBar";
import Intro from "./components/Intro/Intro";
import Portfolio from "./components/portfolio/Portfolio";
import Work from "./components/work/Work";
import Contact from "./components/contact/Contact";
import Menu from "./components/menu/Menu";
import Admin from "./components/privateRoute/Admin";
import Login from "./components/login/Login";
import Registration from "./components/login/Registration";
import { useState } from "react";
import PrivateRoute from "./components/privateRoute/PrivateRoute";
import UserEntry from "./components/privateRoute/UserEntry";
import AdminPortfolio from "./components/privateRoute/AdminPortfolio/AdminPortfolio";
import Resume from "./components/privateRoute/Resume";
import ViewAdPortfolio from "./components/privateRoute/AdminPortfolio/ViewAdPortfolio";
import AddPortfolio from "./components/privateRoute/AdminPortfolio/AddPortfolio";
import EditPortfolio from "./components/privateRoute/AdminPortfolio/EditPortfolio";
import { AnimatePresence } from "framer-motion";

function App() {
  const location = useLocation();
  const [menuOpen, setMenuOpen] = useState(false);

  return (
    <div className="app">
      <NavBar menuOpen={menuOpen} setMenuOpen={setMenuOpen} />
      <Menu menuOpen={menuOpen} setMenuOpen={setMenuOpen} />
      <AnimatePresence>
        <Switch location={location} key={location.key}>
          <section className="sec">
            <Route exact path="/" component={Intro}></Route>
            <Route exact path="/work" component={Work}></Route>
            <Route exact path="/portfolio" component={Portfolio}></Route>
            <Route exact path="/contact" component={Contact}></Route>
            <Route exact path="/login" component={Login}></Route>
            <Route exact path="/registration" component={Registration}></Route>

            {/* Admin route */}
            <PrivateRoute exact path="/admin">
              <Admin />
            </PrivateRoute>

            {/* User Entires Route */}
            <PrivateRoute exact path="/admin/userEntry">
              <UserEntry />
            </PrivateRoute>

            {/* Admin Portfolio Route */}
            <PrivateRoute exact path="/admin/admin-portfolio">
              <AdminPortfolio />
            </PrivateRoute>
            <PrivateRoute exact path="/admin/admin-portfolio/:id">
              <ViewAdPortfolio />
            </PrivateRoute>
            <PrivateRoute exact path="/admin/add-portfolio">
              <AddPortfolio />
            </PrivateRoute>

            <PrivateRoute exact path="/admin/edit-portfolio/:id">
              <EditPortfolio />
            </PrivateRoute>

            {/* Resume Route */}
            <PrivateRoute exact path="/admin/resume">
              <Resume />
            </PrivateRoute>
          </section>
        </Switch>
      </AnimatePresence>
    </div>
  );
}

export default App;
