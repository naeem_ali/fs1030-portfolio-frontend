# FS1040-Portfolio-Frontend

This Repository is the Frontend Part of my Portfolio web application.
You can view a live demo over at [http://portfolio.naeemali.me/](http://portfolio.naeemali.me/).
login as admin user please follow this link [http://portfolio.naeemali.me/login](http://portfolio.naeemali.me/login)

or register as new user

## CI/CD

To run continuous integration and delivery please open the link to Gitlab
repo [https://gitlab.com/naeem_ali/fs1030-portfolio-frontend](https://gitlab.com/naeem_ali/fs1030-portfolio-frontend) and go to solution branch and merge it to the main branch. CI/CD pipeline only includes build and deploy stages test stage is not included in CI/CD pipeline.

# `General functionality:`

- Users can view general pages (Home, Work, Portfolio, Contact)
- User can contact admin user
- Login page (Authenticate users via JWT)
- Admin page Naviagetion (UserEntries, Portfolio, Resume, Logout

### `CRUD Operation`
- User Entires (Read users comments)
- Portfolio (Create, Read, Update and Delete portfolio)
- Resume (Working on it. in future admin user can perform CRUD operation and users can download pdf Resume)

